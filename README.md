# AudiusCraft 

AudiusCraft is a free and open-source mod for Minecraft that implements [Audius Music Service](https://audius.co) in Jukeboxes

## How to install

You will need [Fabric Loader and Fabric API](https://fabricmc.net/use/installer/) installed in your game in order to load AudiusCraft.

## How to use

To play music from Audius, you need to get a disc item with Audius metadatas by a chat command with track URL

Example :
``
/audius Player1234 "https://audius.co/mattox/bet"
``

**Note : Don't forget quotes with URL**

You will get a disk tagged with the track's title. Insert the disk in a jukebox and enjoy !


## FAQ

### The mod is required Client and Server

#### Server Side
The mod is required server-side to generate tagged disks.

#### Client Side
The mod is optional client-side. Clients without the mod will hear nothing with a tagged disk is played.

### Bukkit Version

Comming Soon... 

### Forge Version

No Minecraft Forge version is planned. Do it yourself :)

## License

AudiusCraft is licensed under GNU LGPLv3, a free and open-source license. For more information, please see the license file.