package net.jussak.audiuscraft;

public class Config {
    private String audiusHost = "audius-metadata-5.figment.io";
    private int commandPermissionLevel = 4;

    public String getAudiusHost() {
        return audiusHost;
    }

    public int getCommandPermissionLevel() {
        return Math.max(0, Math.min(4, commandPermissionLevel));
    }
}
