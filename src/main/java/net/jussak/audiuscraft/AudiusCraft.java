package net.jussak.audiuscraft;

import com.google.gson.Gson;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.jussak.audiuscraft.audius.AudiusAPI;
import net.jussak.audiuscraft.audius.Track;
import net.jussak.audiuscraft.utils.DiskUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.string;
import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;
import static net.minecraft.commands.arguments.EntityArgument.getPlayer;
import static net.minecraft.commands.arguments.EntityArgument.player;

public class AudiusCraft implements ModInitializer {
    public static final ResourceLocation MUSIC_LOCATION = new ResourceLocation("audiuscraft", "music");
    public static final ResourceLocation MUSIC_PACKET_ID = new ResourceLocation("audiuscraft", "play");
    private static AudiusCraft instance;
    private Config config = new Config();
    private AudiusAPI audiusAPI;

    public AudiusCraft() {
        instance = this;
    }

    @Override
    public void onInitialize() {
        File configFile = new File("config/audiuscraft.json");

        Gson gson = new Gson();
        try {
            if (configFile.exists()) {
                config = gson.fromJson(new FileReader(configFile), Config.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FileWriter fileWriter = new FileWriter(configFile);
            gson.toJson(config, Config.class, fileWriter);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        audiusAPI = new AudiusAPI(config.getAudiusHost());

        CommandRegistrationCallback.EVENT.register(((dispatcher, registryAccess, environment) -> dispatcher.register(literal("audius").requires(source -> source.hasPermission(config.getCommandPermissionLevel())).then(argument("player", player()).then(argument("url", string()).executes(c -> giveDisk(getString(c, "url"), getPlayer(c, "player"))))))));
    }

    public int giveDisk(String link, ServerPlayer player) {
        Track track = null;

        try {
            track = audiusAPI.getTrackFromPermalink(URLDecoder.decode(link, StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (track == null) {
            player.sendSystemMessage(Component.literal("Track not found"));
        } else {
            Item item = Items.MUSIC_DISC_STRAD;

            if (track.getColor() != null) {
                item = DiskUtils.getDiskFromColor(track.getColor());
            }

            ItemStack itemStack = new ItemStack(item);
            itemStack.setHoverName(Component.literal(track.toString()));
            CompoundTag tag = itemStack.getTag();
            if (tag == null) {
                tag = new CompoundTag();
                itemStack.setTag(tag);
            }

            tag.putString("audiusID", track.getId());
            if (track.getColor() != null) {
                tag.putInt("audiusColor", track.getColor().getRGB());
            }

            player.addItem(itemStack);
        }

        return 1;
    }

    public Config getConfig() {
        return config;
    }

    public AudiusAPI getAudiusAPI() {
        return audiusAPI;
    }

    public static AudiusCraft getInstance() {
        return instance;
    }
}
