package net.jussak.audiuscraft.audius;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class AudiusAPI {
    private final HttpClient httpClient;
    private final String host;
    private static final String APP_NAME = "audiuscraft";
    private final Gson gson = new Gson();

    public AudiusAPI(String host) {
        this.host = host;
        this.httpClient = HttpClient.newBuilder().build();
    }

    private URIBuilder createBuilder() {
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme("https");
        uriBuilder.setHost(this.host);
        uriBuilder.setParameter("app_name", APP_NAME);

        return uriBuilder;
    }
    public JsonObject callAPI(URI uri) throws IOException, InterruptedException {
        HttpRequest httpRequest = HttpRequest.newBuilder(uri).GET().build();
        HttpResponse<String> response = this.httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        return gson.fromJson(response.body(), JsonObject.class);
    }


    public URL getTrackStreamURL(String trackID) throws URISyntaxException, MalformedURLException {
        URIBuilder uriBuilder = this.createBuilder();
        uriBuilder.setPath("/v1/tracks/" + trackID + "/stream");

        return uriBuilder.build().toURL();
    }

    public Track getTrackFromPermalink(String permalink) throws URISyntaxException, IOException, InterruptedException {
        URIBuilder uriBuilder = this.createBuilder();
        uriBuilder.setPath("/v1/tracks");
        uriBuilder.setParameter("permalink", permalink);

        JsonArray jsonArray = this.callAPI(uriBuilder.build()).getAsJsonArray("data");

        return (jsonArray == null || jsonArray.isEmpty()) ? null : new Track(jsonArray.get(0).getAsJsonObject());
    }

}
