package net.jussak.audiuscraft.audius;

import com.google.gson.JsonObject;
import net.jussak.audiuscraft.utils.ImageUtils;

import java.awt.*;
import java.io.IOException;

public class Track {
    private final String id;
    private final String trackName;
    private final String author;

    private Color color = null;

    public Track(String id, String trackName, String author) {
        this.id = id;
        this.trackName = trackName;
        this.author = author;
    }

    public Track(JsonObject jsonObject) {
        this(jsonObject.get("id").getAsString(), jsonObject.get("title").getAsString(), jsonObject.getAsJsonObject("user").get("name").getAsString());

        String artworkURL = jsonObject.getAsJsonObject("artwork").get("150x150").getAsString();

        try {
            color = ImageUtils.getAverageColorFromURL(artworkURL, 150, 150);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getAuthor() {
        return author;
    }
    @Override
    public String toString() {
        return String.format("%s - %s", this.author, this.trackName);
    }

    public Color getColor() {
        return color;
    }
}
