package net.jussak.audiuscraft.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.jussak.audiuscraft.AudiusCraft;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.phys.Vec3;

import static net.jussak.audiuscraft.AudiusCraft.MUSIC_LOCATION;

public class AudiusCraftClient implements ClientModInitializer {

    public static String lastTrackCalled = "";

    @Override
    public void onInitializeClient() {
        ClientPlayNetworking.registerGlobalReceiver(AudiusCraft.MUSIC_PACKET_ID, (client, handler, buf, responseSender) -> {
            lastTrackCalled = buf.readUtf();
            Component trackName = buf.readComponent();
            BlockPos blockPos = buf.readBlockPos();

            client.gui.setNowPlaying(trackName);

            Vec3 vec3 = Vec3.atCenterOf(blockPos);

            SoundInstance soundInstance = new AudiusSoundInstance(MUSIC_LOCATION, SoundSource.RECORDS,  vec3.x, vec3.y, vec3.z);

            client.levelRenderer.playingRecords.put(blockPos, soundInstance);
            client.getSoundManager().play(soundInstance);
        });
    }
}
