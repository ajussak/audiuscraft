package net.jussak.audiuscraft.client;

import net.minecraft.client.resources.sounds.AbstractSoundInstance;
import net.minecraft.client.resources.sounds.Sound;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.client.sounds.SoundManager;
import net.minecraft.client.sounds.WeighedSoundEvents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.valueproviders.ConstantFloat;

public class AudiusSoundInstance extends AbstractSoundInstance {
    protected AudiusSoundInstance(ResourceLocation resourceLocation, SoundSource soundSource, double d, double e, double h) {
        super(resourceLocation, soundSource, SoundInstance.createUnseededRandom());
        this.volume = 1f;
        this.pitch = 1f;
        this.x = d;
        this.y = e;
        this.z = h;
        this.looping = false;
        this.delay = 0;
        this.attenuation = Attenuation.LINEAR;
        this.relative = false;

        this.sound = new Sound(this.location.toString(), ConstantFloat.of(1f), ConstantFloat.of(1f), 1, Sound.Type.FILE, true, false, 16);
    }

    @Override
    public WeighedSoundEvents resolve(SoundManager soundManager) {
        return SoundManager.INTENTIONALLY_EMPTY_SOUND_EVENT;
    }
}
