package net.jussak.audiuscraft.client;

import javazoom.jl.decoder.*;
import net.minecraft.client.sounds.AudioStream;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.BufferUtils;

import javax.sound.sampled.AudioFormat;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;

/**
 * MP3 Decoding code extracted from : https://github.com/libgdx/libgdx
 */
public class Mp3AudioStream implements AudioStream {

    private Bitstream bitstream;
    private OutputBuffer outputBuffer;
    private MP3Decoder decoder;
    private final URL url;
    private AudioFormat audioFormat;

    public Mp3AudioStream(URL url) throws IOException {
        this.url = url;

        bitstream = new Bitstream(url.openStream());
        decoder = new MP3Decoder();
        try {
            Header header = bitstream.readFrame();
            if (header == null) throw new IOException("Empty MP3");
            int channels = header.mode() == Header.SINGLE_CHANNEL ? 1 : 2;
            outputBuffer = new OutputBuffer(channels, false);
            decoder.setOutputBuffer(outputBuffer);
            this.audioFormat = new AudioFormat(header.getSampleRate(), 16, channels, true, false);
        } catch (BitstreamException e) {
            throw new IOException("error while preloading mp3", e);
        }
    }

    @Override
    public @NotNull AudioFormat getFormat() {
        return audioFormat;
    }

    @Override
    public @NotNull ByteBuffer read(int i) throws IOException {
        try {
            boolean setup = bitstream == null;
            if (setup) {
                bitstream = new Bitstream(url.openStream());
                decoder = new MP3Decoder();
            }

            int totalLength = 0;
            int minRequiredLength = i - OutputBuffer.BUFFERSIZE * 2;
            ByteBuffer byteBuffer = BufferUtils.createByteBuffer(i + 1 & 0xFFFFFFFE);
            while (totalLength <= minRequiredLength) {
                Header header = bitstream.readFrame();
                if (header == null) break;
                if (setup) {
                    int channels = header.mode() == Header.SINGLE_CHANNEL ? 1 : 2;
                    outputBuffer = new OutputBuffer(channels, false);
                    decoder.setOutputBuffer(outputBuffer);
                    this.audioFormat = new AudioFormat(header.getSampleRate(), 16, channels, true, false);
                    setup = false;
                }
                try {
                    decoder.decodeFrame(header, bitstream);
                } catch (Exception ignored) {
                    // JLayer's decoder throws ArrayIndexOutOfBoundsException sometimes!?
                }
                bitstream.closeFrame();

                int length = outputBuffer.reset();
                byteBuffer.put(outputBuffer.getBuffer(), 0, length);
                totalLength += length;
            }
            byteBuffer.flip();
            return byteBuffer;
        } catch (Throwable ex) {
            close();
            throw new IOException("Error reading audio data.", ex);
        }
    }

    @Override
    public void close() throws IOException {
        if (bitstream == null)
            return;

        try {
            bitstream.close();
        } catch (BitstreamException e) {
            throw new IOException(e);
        }
        bitstream = null;
    }
}
