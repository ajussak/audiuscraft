package net.jussak.audiuscraft.utils;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class DiskUtils {
    public static final Map<Color, Item> colorItemMap = new HashMap<>();

    static {
        colorItemMap.put(new Color(0x89c114), Items.MUSIC_DISC_WARD);
        colorItemMap.put(new Color(0x638ecb), Items.MUSIC_DISC_WAIT);
        colorItemMap.put(new Color(0xffffff), Items.MUSIC_DISC_STRAD);
        colorItemMap.put(new Color(0x176e46), Items.MUSIC_DISC_OTHERSIDE);
        colorItemMap.put(new Color(0xbf90eb), Items.MUSIC_DISC_MELLOHI);
        colorItemMap.put(new Color(0x7656e2), Items.MUSIC_DISC_MALL);
        colorItemMap.put(new Color(0x5fdd6a), Items.MUSIC_DISC_FAR);
        colorItemMap.put(new Color(0x7f0002), Items.MUSIC_DISC_CHIRP);
        colorItemMap.put(new Color(0x4cff00), Items.MUSIC_DISC_CAT);
        colorItemMap.put(new Color(0xe2543b), Items.MUSIC_DISC_BLOCKS);
        colorItemMap.put(new Color(0xffd800), Items.MUSIC_DISC_13);
    }

    public static Item getDiskFromColor(Color color) {
        return colorItemMap.get(ImageUtils.getNearestColor(colorItemMap.keySet(), color));
    }
}
