package net.jussak.audiuscraft.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Set;

public class ImageUtils {

    public static Color getNearestColor(Set<Color> colorSet, Color targetColor) {
        Color nearest = null;

        for (Color color : colorSet) {
            if (nearest == null || colorDistance(nearest, targetColor) > colorDistance(color, targetColor)) {
                nearest = color;
            }
        }

        return nearest;
    }

    public static Color getAverageColorFromURL(String url, int w, int h) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new URL(url));
        return averageColor(bufferedImage, 0, 0, w, h);
    }

    public static double colorDistance(Color c1, Color c2) {
        int red1 = c1.getRed();
        int red2 = c2.getRed();
        int rmean = (red1 + red2) >> 1;
        int r = red1 - red2;
        int g = c1.getGreen() - c2.getGreen();
        int b = c1.getBlue() - c2.getBlue();
        return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
    }


    public static Color averageColor(BufferedImage bi, int x0, int y0, int w, int h) {
        int x1 = x0 + w;
        int y1 = y0 + h;
        long sumr = 0, sumg = 0, sumb = 0;
        for (int x = x0; x < x1; x++) {
            for (int y = y0; y < y1; y++) {
                Color pixel = new Color(bi.getRGB(x, y));
                sumr += pixel.getRed();
                sumg += pixel.getGreen();
                sumb += pixel.getBlue();
            }
        }
        int num = w * h;
        return new Color((int) sumr / num, (int) sumg / num, (int) sumb / num);
    }

}
