package net.jussak.audiuscraft.mixin;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.jussak.audiuscraft.AudiusCraft;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.JukeboxBlockEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(JukeboxBlockEntity.class)
public class JukeboxMixin {

    @Redirect(at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/Level;levelEvent(Lnet/minecraft/world/entity/player/Player;ILnet/minecraft/core/BlockPos;I)V"), method = "startPlaying")
    private void gameEvent(Level instance, Player player, int i, BlockPos blockPos, int id) {
        JukeboxBlockEntity that = ((JukeboxBlockEntity) (Object) this);

        ItemStack itemStack = that.getFirstItem();

        CompoundTag compoundTag = itemStack.getTag();

        if (compoundTag != null && compoundTag.contains("audiusID")) {
            for (ServerPlayer otherPlayer : PlayerLookup.tracking((ServerLevel) instance, that.getBlockPos())) {
                FriendlyByteBuf packetByteBufs = PacketByteBufs.create();
                packetByteBufs.writeUtf(compoundTag.getString("audiusID"));
                packetByteBufs.writeComponent(itemStack.getHoverName());
                packetByteBufs.writeBlockPos(that.getBlockPos());
                ServerPlayNetworking.send(otherPlayer, AudiusCraft.MUSIC_PACKET_ID, packetByteBufs);
            }
        } else {
            instance.levelEvent(player, i, blockPos, id);
        }
    }

}
