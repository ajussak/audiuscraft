package net.jussak.audiuscraft.mixin;

import net.jussak.audiuscraft.AudiusCraft;
import net.jussak.audiuscraft.audius.AudiusAPI;
import net.jussak.audiuscraft.client.AudiusCraftClient;
import net.jussak.audiuscraft.client.Mp3AudioStream;
import net.minecraft.client.sounds.AudioStream;
import net.minecraft.client.sounds.SoundBufferLibrary;
import net.minecraft.resources.ResourceLocation;
import org.spongepowered.asm.mixin.Debug;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

@Mixin(SoundBufferLibrary.class)
@Debug(export = true)
public class SoundBufferLibraryMixin {

    @Inject(at = @At(value = "HEAD"), method = "method_19745", cancellable = true)
    public void injected(ResourceLocation resourceLocation, boolean bl, CallbackInfoReturnable<AudioStream> cir) {
        if (resourceLocation.getNamespace().equals("audiuscraft")) {
            try {
                AudiusAPI audiusAPI = AudiusCraft.getInstance().getAudiusAPI();
                cir.setReturnValue(new Mp3AudioStream(audiusAPI.getTrackStreamURL(AudiusCraftClient.lastTrackCalled)));
                cir.cancel();
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

}
